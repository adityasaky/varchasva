<!DOCTYPE html>
<html lang="en">

<?php
include 'includes/head.php';
?>

<body>

  <?php
  include 'includes/nav.php';
  ?>

  <!-- Header for the page -->
  <header class="jumbotron hero-spacer">
      <img src="images/aaron.png" width="50%">
      <h1>About</h1>
  </header>

  <!-- Page Content -->
  <div class="container">

    <p>Spread across 13 acres the SJBIT is located amidst peace and tranquility. SJBIT is committed to personal, social & intellectual growth emphasizing a comprehensive educational programme leading to high academic standards.</p>

    <h2>Contact Us</h2>

    <a href="tel:+917760196991" class="btn btn-success">+91 77601 96991</a>
    <a href="tel:+919743268585" class="btn btn-success">+91 97432 68585</a>

  </div>
  <!-- /.container -->
  <?php
  include 'includes/footer.php';
  ?>
  <?php
  include 'includes/js.php';
  ?>
</body>

</html>
