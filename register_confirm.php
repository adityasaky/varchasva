<!DOCTYPE html>
<html lang="en">

<?php
include 'includes/head.php';
require 'includes/settings.php';
$regid = $_GET['regid'];
$regid = $conn->real_escape_string($regid);
$sql = "SELECT * FROM registrations WHERE ID='$regid'";
$reg = mysqli_query($conn, $sql);
$reg = mysqli_fetch_object($reg);
$event = $reg->Event;
$sql = "SELECT * FROM events WHERE ID='$event'";
$event = mysqli_query($conn, $sql);
$event = mysqli_fetch_object($event);
$sql = "SELECT * FROM participants WHERE RegID='$regid'";
$participants = mysqli_query($conn, $sql);
?>

<body>

  <?php
  include 'includes/nav.php';
  ?>

  <!-- Header for the page -->
  <header class="jumbotron hero-spacer">
      <img src="images/aaron.png" width="50%">
      <h1>Registration Confirmed!</h1>
  </header>

  <!-- Page Content -->
  <div class="container">
    <h1>Hi!</h1>
    <h2>Your registration for <?=$event->Event ?> has been confirmed! Here are the details:</h2>
    <h3>Participants:</h3>
    <h4>
    <table class="table">
        <thead>
            <th>Name</th>
            <th>College</th>
        </thead>
        <tbody>
        <?php
            while ($row = mysqli_fetch_object($participants)) {
        ?>
        <tr>
            <td><?=$row->Name ?></td>
            <td><?=$row->College ?></td>
        </tr>
        <?php
            }
        ?>
    </tbody>
    </table>
    </h4>
    <h3>Registration ID</h3>
    <h4>
        <?=$regid ?>
        <br><br>
        Use this ID to check in.
    </h4>
    <h3>Rules and Regulations:</h3>
        <h4>
        <ul>
            <li>SJB Institute of Technology will not participate in any of the main stage events.</li>
            <br>
            <li>The decision of the judges and management is final in all cases.</li>
        </ul>
        </h4>

  </div>
  <!-- /.container -->
  <?php
  include 'includes/footer.php';
  ?>
  <?php
  include 'includes/js.php';
  ?>
</body>

</html>
