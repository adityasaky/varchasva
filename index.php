<!DOCTYPE html>
<html lang="en">

<?php
include 'includes/head.php';
?>

<body>

  <?php
  include 'includes/nav.php';
  ?>

  <!-- Jumbotron Header -->
  <header class="jumbotron hero-spacer">
      <div class="header-noblur">
        <img src="images/aaron.png" width="50%">
      <h1></h1>
      <div class="register"><p><a href="register" class="btn-landing">Register</a></div>
</div>
  </header>

  <!--<hr>-->
  <!-- Page Content -->
  <div class="container">

    <!-- Information snippets -->
    <div class="row text-center">

      <div class="col-md-3 col-sm-6 hero-feature">
        <div class="thumbnail">
          <div class="caption">
            <h3>Register</h3>
            <p>Interested in signing up? Register for Varchasva 2016 now!</p>
            <p>
              <a href="register" class="btn btn-primary">Register</a>
            </p>
          </div>
        </div>
      </div>

      <div class="col-md-3 col-sm-6 hero-feature">
        <div class="thumbnail">
          <div class="caption">
            <h3>Events</h3>
            <p>Find all our exciting events here!</p>
            <p>
              <a href="events" class="btn btn-warning">Events</a>
            </p>
          </div>
        </div>
      </div>

      <div class="col-md-3 col-sm-6 hero-feature">
        <div class="thumbnail">
          <div class="caption">
            <h3>Where?</h3>
            <p>Varchasva 2016 will be held at SJB Institute of Technology, Kengeri, Bangalore.</p>
            <p>
              <a href="https://www.google.co.in/maps/place/SJB+Institute+of+Technology/@12.8998841,77.4957882,15z/data=!4m2!3m1!1s0x0:0x46277e17c8b2ccc3?sa=X&ved=0ahUKEwjkzuSc9IXMAhVDI5QKHdnEAD0Q_BIIcDAQ" class="btn btn-success" target="_blank">Locate on Google Maps</a>
            </p>
          </div>
        </div>
      </div>

      <div class="col-md-3 col-sm-6 hero-feature">
        <div class="thumbnail">
          <div class="caption">
            <h3>When?</h3>
            <p>Varchasva 2016 will be held on the 23rd and 24th of April, 2016.</p>
            <p><a href="about" class="btn btn-primary">Contact us for more details</a>
            </p>
          </div>
        </div>
      </div>

    </div>
    <!-- /.row -->

  </div>
  <!-- /.container -->
  <?php
  include 'includes/footer.php';
  ?>
  <?php
  include 'includes/js.php';
  ?>
</body>

</html>
