<!DOCTYPE html>
<html lang="en">

<?php
include 'includes/head.php';
require 'includes/settings.php';
?>

<body>

  <?php
  include 'includes/nav.php';
  ?>

  <!-- Header for the page -->
  <header class="jumbotron hero-spacer">
      <img src="images/aaron.png" width="50%">
      <h1>Register</h1>
      <p>Register for Varchasva 2016 now!</p>
  </header>

  <!-- Page Content -->
  <div class="container">
   <form method="post" action="action/register.php">

    <div class="input-group col-md-6">
        <select name="event" class="form-control">
            <?php
                $events = mysqli_query($conn, "SELECT * FROM events");
                while ($row = mysqli_fetch_object($events)) {
            ?>
            <option value=<?=$row->ID ?>><?=$row->Event ?></option>
            <?php
            }
            ?>
        </select>
    </div>
    <br>
    <div class="departments">
    <div class="department">
    <div class="input-group col-md-6">
        <input type="text" name="participant[]" placeholder="Name of Participant" class="form-control" required>
    </div>
    <br>
    <div class="input-group col-md-6">
        <input placeholder="College" type="text" name="college[]" class="form-control" required>
    </div>
    <br>
    <div class="input-group col-md-6">
        <input placeholder="Email" type="email" name="email[]" class="form-control" required>
    </div>
    <br>
    <div class="input-group col-md-6">
        <input placeholder="Phone Number" type="text" name="phone[]" class="form-control" required>
    </div>
    <br>
    </div>
    </div>
    <div class="input-group">
        <button class="add_field_button btn btn-default">Add another participant</button>
    </div>
    <br>
    <input type="submit" class="btn btn-lg btn-success" value="Register">
   </form>
  </div>
  <!-- /.container -->
  <?php
  include 'includes/footer.php';
  ?>
  <?php
  include 'includes/js.php';
  ?>
</body>

</html>
