<!DOCTYPE html>
<html lang="en">

<?php
include 'includes/head.php';
require 'includes/settings.php';
?>

<body>

  <?php
  include 'includes/nav.php';
  ?>

  <!-- Jumbotron Header -->
  <header class="jumbotron hero-spacer">
      <img width="50%" src="images/aaron.png">
      <h1>Events</h1>
  </header>

  <!--<hr>-->
  <!-- Page Content -->
  <div class="container">

    <?php
        $sql = "SELECT * FROM events WHERE Day='1' ORDER BY Time";
        $events = mysqli_query($conn, $sql);
    ?>
<h2>Day 1</h2>
<table class="table">
<thead>
<th>Event</th>
<th>Venue</th>
<th>Time (24 hrs)</th>
<th>Cost</th>
<th>Number of Participants</th>
</thead>
<tbody>
<?php
        while ($row = mysqli_fetch_object($events)) {
?>
<tr>
<td><?=$row->Event ?></td>
<td><?=$row->Venue ?></td>
<td><?=$row->Time ?></td>
<td><?=$row->Cost ?></td>
<td><?php if ($row->Number == 0) { echo 'N/A'; } else { echo $row->Number; } ?></td>
</tr>
<?php
        }
?>
</tbody>
</table>

<?php
    $sql = "SELECT * FROM events WHERE Day='2' ORDER BY Time";
    $events = mysqli_query($conn, $sql);
?>
<h2>Day 2</h2>
<table class="table">
<thead>
<th>Event</th>
<th>Venue</th>
<th>Time (24 hrs)</th>
<th>Cost</th>
<th>Number of Participants</th>
</thead>
<tbody>
<?php
    while ($row = mysqli_fetch_object($events)) {
?>
<tr>
<td><?=$row->Event ?></td>
<td><?=$row->Venue ?></td>
<td><?=$row->Time ?></td>
<td><?=$row->Cost ?></td>
<td><?php if ($row->Number == 0) { echo 'N/A'; } else { echo $row->Number; } ?></td>
</tr>
<?php
    }
?>
</tbody>
</table>

  </div>
  <!-- /.container -->
  <?php
  include 'includes/footer.php';
  ?>
  <?php
  include 'includes/js.php';
  ?>
</body>

</html>
