<?php
// Navigation on the left side for dashboard.php
?>
<div class="col-sm-3 col-md-2 sidebar">
  <ul class="nav nav-sidebar">
    <li><a href="index.php">Dashboard Home</a></li>
    <li><a href="events.php">Add Event</a></li>
    <li><a href="viewevents.php">View Events</a></li>
    <li><a href="logout.php">Log Out</a></li>
  </ul>
</div>
