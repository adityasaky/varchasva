<?php
// Main dashboard
session_start();
$settings = require '../includes/settings.php';
if ($_SESSION['login_check'] == 1) {
  $user_id = $_SESSION['user_id'];
  ?>
  <!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Dashboard</title>

    <!-- Bootstrap core CSS -->
    <link href="./includes/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="./includes/dashboard.css" rel="stylesheet">

  </head>

  <body>

    <?php
    include './includes/top_nav.php';

    ?>
    <div class="container-fluid">
      <div class="row">
        <?php
        include './includes/left_nav.php'
        ?>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">Dashboard</h1>

          <h2 class="sub-header">View Events</h2>
          <?php
            $sql = "SELECT * FROM events ORDER BY Day";
            $events = mysqli_query($conn, $sql);
          ?>
          <div class="table-stripped">
            <table class="table">
              <thead>
                <th>Name</th>
                <th>Day</th>
                <th>Cost</th>
                <th>Venue</th>
                <th>View</th>
              </thead>
              <tbody>
                <?php
                  while ($event = mysqli_fetch_object($events)) {
                ?>
                <tr>
                  <td><?=$event->Event ?></td>
                  <td><?=$event->Day ?></td>
                  <td><?=$event->Cost ?></td>
                  <td><?=$event->Venue ?></td>
                  <td><a href="viewparticipants.php?id=<?=$event->ID ?>">View</a></td>
                </tr>
                <?php
              }
                ?>
          </div>
          </div>
        </div>
      </div>
      <?php include 'includes/js.php'; ?>
    </body>
    </html>
    <?php

  }

  else {
    $_SESSION['login_message'] = "Login to view this page.";
    header('Location: signin.php');
  }
