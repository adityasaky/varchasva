<?php
// Main dashboard
session_start();
$settings = require '../includes/settings.php';
if ($_SESSION['login_check'] == 1) {
  $user_id = $_SESSION['user_id'];
  ?>
  <!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Dashboard</title>

    <!-- Bootstrap core CSS -->
    <link href="./includes/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="./includes/dashboard.css" rel="stylesheet">

  </head>

  <body>

    <?php
    include './includes/top_nav.php';

    ?>
    <div class="container-fluid">
      <div class="row">
        <?php
        include './includes/left_nav.php'
        ?>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">Dashboard</h1>

          <h2 class="sub-header">Search</h2>
          <form method="post" action="search.php">
            <div class="input-group col-md-6">
              <input type="text" placeholder="Enter registration ID" name="regid" class="form-control" required>
            </div>
            <br>
            <div class="input-group">
              <input type="submit" value="Search" class="btn btn-success">
            </div>
          </div>
        </div>
      </div>
      <?php include 'includes/js.php'; ?>
    </body>
    </html>
    <?php

  }

  else {
    $_SESSION['login_message'] = "Login to view this page.";
    header('Location: signin.php');
  }
