<?php
// Main dashboard
session_start();
$settings = require '../includes/settings.php';
if ($_SESSION['login_check'] == 1) {
  $user_id = $_SESSION['user_id'];
  ?>
  <!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Dashboard</title>

    <!-- Bootstrap core CSS -->
    <link href="./includes/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="./includes/dashboard.css" rel="stylesheet">

  </head>

  <body>

    <?php
    include './includes/top_nav.php';

    ?>
    <div class="container-fluid">
      <div class="row">
        <?php
        include './includes/left_nav.php'
        ?>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">Dashboard</h1>

          <h2 class="sub-header">Search</h2>

          <?php
          $regid = $_POST['regid'];
          $sql = "SELECT * FROM registrations WHERE ID='$regid'";
          $details = mysqli_query($conn, $sql);
          $sql = "SELECT * FROM participants WHERE RegID='$regid'";
          $participants = mysqli_query($conn, $sql);
          $registrations = mysqli_fetch_object($details);
          $eventID = $registrations->Event;
          $sql = "SELECT * FROM events WHERE ID='$eventID'";
          $event = mysqli_query($conn, $sql);
          $event = mysqli_fetch_object($event);
          if (mysqli_num_rows($details) != 0) {
            ?>

            <h3 class="sub-header">Event</h3>
            <div class="table-stripped">
              <table class="table">
                <thead>
                  <th>Category</th>
                  <th>Details</th>
                </thead>
                <tbody>
                  <tr>
                  <td>Event</td>
                <td><?=$event->Event ?></td>
              </tr>
              <tr>
                <td>Venue</td>
                <td><?=$event->Venue ?></td>
              </tr>
              <tr>
                <td>Cost</td>
                <td><?=$event->Cost ?></td>
              </tr>
            </tbody>
              </table>
            </div>
            <h3 class="sub-header">Participants</h3>
            <div class="table-stripped">
              <table class="table">
                <thead>
                  <th>Name</th>
                  <th>College</th>
                  <th>Phone Number</th>
                  <th>Email</th>
                </thead>
                <tbody>
                  <?php
                  while ($row = mysqli_fetch_object($participants)) {
                    ?>
                    <tr>
                      <td><?=$row->Name ?></td>
                      <td><?=$row->College ?></td>
                      <td><?=$row->Phone ?></td>
                      <td><?=$row->Email ?></td>
                    </tr>

                    <?php
                  }
                  ?>
                </div>
                <?php
              }
              else {
                echo '<h3>Invalid registration.</h3>';
              }
              ?>
            </div>
          </div>
          <?php include './includes/js.php'; ?>
        </body>
        </html>
        <?php

      }

      else {
        $_SESSION['login_message'] = "Login to view this page.";
        header('Location: signin.php');
      }
