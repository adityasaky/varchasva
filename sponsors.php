<!DOCTYPE html>
<html lang="en">

<?php
include 'includes/head.php';
?>

<body>

  <?php
  include 'includes/nav.php';
  ?>

  <!-- Jumbotron Header -->
  <header class="jumbotron hero-spacer">
      <img width="50%" src="images/aaron.png">
      <h1>Sponsors</h1>
      <p>These guys made it all happen!</p>
  </header>

  <!--<hr>-->
  <!-- Page Content -->
  <div class="container">

    <!-- Information snippets -->
    <div class="row text-center">

      <div class="col-md-4 col-sm-8 hero-feature">
        <div class="thumbnail">
          <div class="caption">
            <h3>Kanva Group</h3>
            <img src="sponsors/1.jpg">
          </div>
        </div>
      </div>

      <div class="col-md-4 col-sm-8 hero-feature">
        <div class="thumbnail">
          <div class="caption">
            <h3>Lankesh Patrike</h3>
            <img src="sponsors/2.jpg" width="40%">
          </div>
        </div>
      </div>

      <div class="col-md-4 col-sm-8 hero-feature">
        <div class="thumbnail">
          <div class="caption">
            <h3>Vijayavani</h3>
            <img src="sponsors/3.jpg" width="50%">
          </div>
        </div>
      </div>

    </div>
    <!-- /.row -->

    <!-- Information snippets -->
    <div class="row text-center">

      <div class="col-md-4 col-sm-8 hero-feature">
        <div class="thumbnail">
          <div class="caption">
            <h3>Radio City</h3>
            <img src="sponsors/4.jpg" width="70%">
          </div>
        </div>
      </div>

      <div class="col-md-4 col-sm-8 hero-feature">
        <div class="thumbnail">
          <div class="caption">
            <h3>YTV India</h3>
            <img src="sponsors/5.jpg" width="50%">
          </div>
        </div>
      </div>

      <div class="col-md-4 col-sm-8 hero-feature">
        <div class="thumbnail">
          <div class="caption">
            <h3>Global Hospitals</h3>
            <img src="sponsors/bgs.jpg" width="60%">
          </div>
        </div>
      </div>

    </div>
    <!-- /.row -->

  </div>
  <!-- /.container -->
  <?php
  include 'includes/footer.php';
  ?>
  <?php
  include 'includes/js.php';
  ?>
</body>

</html>
