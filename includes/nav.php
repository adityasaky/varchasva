<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-static-top" role="navigation">
  <div class="container">
    <!-- Create mobile toggle button and bar -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <!-- Add the horizontal bars to the toggle button -->
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="home">Varchasva '16</a>
    </div>
    <!-- All the links -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li>
          <a href="register">Register</a>
        </li>
        <li>
          <a href="events">Events</a>
        </li>
        <li>
          <a href="sponsor">Sponsors</a>
        </li>
        <li>
          <a href="about">About</a>
        </li>
      </ul>
    </div>
    <!-- /.navbar-collapse -->
  </div>
  <!-- /.container -->
</nav>
