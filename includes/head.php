<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Varchasva '16">
  <meta name="keywords" content="varchasva, sjbit, fest, bangalore, 2016">
  <meta name="author" content="SJB Tech Team">

  <!-- Facebook tags -->
  <meta property="og:title" content="Varchasva '16"/>
  <meta property="og:description" content="Varchasva '16 - The SJBIT Fest"/>
<!--  <meta property="og:image" content="http://freedomhack.fsmk.org/images/og-image.png" />-->
<!--  <meta property="og:url" content="https://freedomhack.fsmk.org/"/>-->
  <meta property="og:site_name" content="Varchasva '16"/>
  <meta property="og:type" content="website"/>
  <!-- end Facbook tags -->

  <title>Varchasva '16</title>

  <!-- Favicon -->
  <link rel="icon" type="image/ico" href="../images/favicon.ico">

  <!-- Font awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

  <!-- Bootstrap Core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom CSS -->
  <link href="css/style.css" rel="stylesheet">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>
